/*
 * Questa classe serve creare un fantasmino
 */
package pacman;

import java.awt.Color;

/**
 *
 * @author BBC
 */
public class Fantasmino {
    private int posR; //la pos attuale sulla riga
    private int posC; //la pos attuale sulla colonna
    private int nextR; //la pos successiva sulla riga
    private int nextC; //la pos successiva sulla colonna
    private Color colore; //memorizzo il colore
    
    
    ///////////////////////////////////////////
    //COSTRUTTORI
    ///////////////////////////////////////////
    public Fantasmino() {
        this.posR = 0;
        this.posC = 0;
        this.nextR = 0;
        this.nextC = 0;
        this.colore = randomColor();
    }
    public Fantasmino(int r, int c) {
        this.posR = r;
        this.posC = c;
        this.nextR = r;
        this.nextC = c;
        this.colore = randomColor(); //Quando viene chiamato questo costruttore
                                     //viene dato un colore casuale
    }
    
    //ritorno un colore casuale tra quelli possibili (num da 0 a 4)
    private Color randomColor(){
       int num = (int)Math.round(Math.random() * 3);       
       if(num == 0){
           return Color.BLUE;
       }
       else if(num == 1){
           return Color.RED;
       }
       else if(num == 2){
           return Color.PINK;
       }
       else{
           return Color.ORANGE;
       }
    }
    ///////////////////////////////////////////
    //SET DI TUTTI GLI ATTRIBUTI
    ///////////////////////////////////////////
    public void setPosR(int r){
        this.posR = r;
    }
    public void setPosC(int c){
        this.posC = c;
    }
    public void setNextR(int c){
        this.nextR = c;
    }
    public void setNextC(int c){
        this.nextC = c;
    }
    public void setPos(int posR, int posC){
        this.posR = posR;
        this.posC = posC;
    }
    public void setNextPos(int posR, int posC){
        this.nextR = nextR;
        this.nextC = nextC;
    }
   public void setColor(Color c){
       this.colore = c;
   }
    ///////////////////////////////////////////
    //GET DI TUTTI GLI ATTRIBUTI
    ///////////////////////////////////////////
    public int getPosR(){
        return this.posR;
    }
    public int getPosC(){
        return this.posC;
    }
    public int getNextR(){
        return this.nextR;
    }
    public int getNextC(){
        return this.nextC;
    }
    public Color getColor(){
        return this.colore;
    }
    
    ///////////////////////////////////////////
    //METODI DI SPOSTAMENTO
    ///////////////////////////////////////////
    public void goLeft(){
        this.posC--;
    }
    public void goRight(){
        this.posC++;
    }
    public void goUp(){
        this.posR--;
    }
    public void goDown(){
        this.posR++;
    }
}