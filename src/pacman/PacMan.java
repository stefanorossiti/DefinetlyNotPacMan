/*
 * Questa classe gestisce il pac-man
 */
package pacman;

/**
 *
 * @author BBC
 */
public class PacMan {
    private int posR;
    private int posC;
    
    ///////////////////////////////////////////
    //COSTRUTTORI
    ///////////////////////////////////////////
    public PacMan(){
    }    
    
    public PacMan(int r, int c){
        this.posR = r;
        this.posC = c;
    }    
    ///////////////////////////////////////////
    //SET DI TUTTI GLI ATTRIBUTI
    ///////////////////////////////////////////
    public void setPosR(int posR){
        this.posR = posR;
    }
    
    public void setPosC(int posC){
        this.posC = posC;
    }
    public void setPos(int posR, int posC){
        this.posR = posR;
        this.posC = posC;
    }
    
    ///////////////////////////////////////////
    //GET DI TUTTI GLI ATTRIBUTI
    ///////////////////////////////////////////
    public int getPosR(){
        return this.posR;
    }
    public int getPosC(){
        return this.posC;
    }
    ///////////////////////////////////////////
    //METODI GENERALI
    ///////////////////////////////////////////
    
    ///////////////////////////////////////////
    //METODI DI SPOSTAMENTO
    ///////////////////////////////////////////
    public void goLeft(){
        this.posC--;
    }
    public void goRight(){
        this.posC++;
    }
    public void goUp(){
        this.posR--;
    }
    public void goDown(){
        this.posR++;
    }
}
