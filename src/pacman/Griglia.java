/*
 * Qui creo la cells che verra stampata
 */
package pacman;

/**
 *
 * @author BBC
 */
public class Griglia {
    private Cella[][] cells; 
    ///////////////////////////////////////////
    //COSTRUTTORI
    ///////////////////////////////////////////
    public Griglia(){
        
    }
    public Griglia(int r, int c){
        cells = new Cella[r][c];
        
        for(int i = 0; i < r; i++){
            for(int j = 0; j < c; j++){
                cells[i][j] = new Cella();
            }
        }
    }
    ///////////////////////////////////////////
    //GET DI TUTTI GLI ATTRIBUTI
    ///////////////////////////////////////////
    public boolean isMuro(int r, int c){
        return this.cells[r][c].isMuro();
    }
    public int getPassi(int r, int c){
        return this.cells[r][c].getPassi();
    }
    ///////////////////////////////////////////
    //SET E TOGGLE DI TUTTI GLI ATTRIBUTI
    /////////////////////////////////////////// 
    public void setStato(int r, int c, String stato){
        this.cells[r][c].setStato(stato);
    }
    public void setPassi(int r, int c){
       
    }
    public void toggleStato(int r, int c){
        if(this.cells[r][c].isMuro() == true){
            this.cells[r][c].setStato("libera");
        }
        else{
            this.cells[r][c].setStato("muro");
        }
    }
    ///////////////////////////////////////////
    //METODI
    /////////////////////////////////////////// 
    
    //ciclo che genera i muri per una griglia 11x15 ovvero del liv 1 di pacman
    public void generaMuriPacman(){
        //inserisco i muri nella griglia
        for(int r = 0; r < cells.length; r++){
            for(int c = 0; c < cells[0].length; c++){
                //metto i muri ai bordi
                if(r == 0 || c == 0 || r == cells.length-1 || c == cells[0].length-1){
                    cells[r][c].setStato("muro");
                }
                //metto i muri alla prima e alla penultima riga
                if((r == 1 || r == cells.length-2) && (c == 5 || c == 14)){
                    cells[r][c].setStato("muro");
                }
                //metto i muri alla seconda e alla terzultima riga
                if((r == 2 || r == cells.length-3) && (c == 2 || c == 3 || c == 5  || c == 14 || c == 16 || c == 17  || (c>6 && c<13))){
                    cells[r][c].setStato("muro");
                }
                //metto i muri alla terza e alla qartultima riga
                if((r == 3 || r == cells.length-4) && (c == 2 || c == 17)){
                    cells[r][c].setStato("muro");
                }
                //metto i muri alla quarta e alla quintultima riga in piu aggiungo 2 quardati in mezzo alla quintultima riga
                if((r == 4 || r == cells.length-5) && (c == 2 || c == 4 || c == 5 || c == 7 || c == 8 || c == 11 || c == 12 || c == 14 || c == 15 || c == 17 || (r == cells.length-5 && (c == 9 || c == 10)))){
                    cells[r][c].setStato("muro");
                }
                //metto i muri alla riga centrale
                if((r == 5) && (c == 7 || c == 12)){
                    cells[r][c].setStato("muro");
                }
            }
        }
    }
    //ciclo che genera i muri per una griglia 16x21 ovvero del liv 2 di pacman
    public void generaMuriPacmanL2(){
        //inserisco i muri nella griglia
        for(int r = 0; r < cells.length; r++){
            for(int c = 0; c < cells[0].length; c++){
                //metto i muri ai bordi
                if(r == 0 || c == 0 || r == cells.length-1 || c == cells[0].length-1){
                    cells[r][c].setStato("muro");
                }
                if(r == 1 && c == 10 ){
                    cells[r][c].setStato("muro");
                }
                if(r == 2 && c == 10 ){
                    cells[r][c].setStato("muro");
                }
                if(r == 2 && (c == 2 || c == 3 || c == 4 || c == 6 || c == 7 || c == 8 || c == 10 || c == 12 || c == 13 || c == 14 || c == 16 || c == 17 || c == 18) ){
                    cells[r][c].setStato("muro");
                }
                if(r == 4 && (c == 2 || c == 3 || c == 4 || c == 6 || c == 8 || c == 9 || c == 10 || c == 11 || c == 12 || c == 14 || c == 16 || c == 17 || c == 18) ){
                    cells[r][c].setStato("muro");
                }
                if(r == 5 && (c == 6 || c == 10 || c == 14) ){
                    cells[r][c].setStato("muro");
                }
                if(r == 6 && (c == 1 || c == 2 || c == 3 || c == 4 || c == 6 || c == 7 || c == 8 || c == 10 || c == 12 || c == 13 || c == 14 || c == 16 || c == 17 || c == 18 || c == 19) ){
                    cells[r][c].setStato("muro");
                }
                if(r == 8 && (c == 8 || c == 13) ){
                    cells[r][c].setStato("muro");
                }
                if(r == 9 && (c == 1 || c == 2 || c == 3 || c == 4 || c == 6 || c == 8 || c == 9 || c == 10 || c == 11 || c == 12 || c == 13 || c == 15 || c == 17 || c == 18 || c == 19) ){
                    cells[r][c].setStato("muro");
                }
                if(r == 11 && ((c >= 8 && c <= 13) || (c >= 1 && c <= 4) || (c >= 16 && c <= 19))){
                    cells[r][c].setStato("muro");
                }
                if(r == 13 && (c == 2 || c == 3 || c == 5 || c == 6 || c == 7 || c == 9 || c == 10 || c == 12 || c == 13 || c == 14 || c == 17 || c == 18) ){
                    cells[r][c].setStato("muro");
                }
            }
        }
    }
    public void generaMuriBordo(){
        for(int r = 0; r < cells.length; r++){
            for(int c = 0; c < cells[0].length; c++){
                //metto i muri ai bordi
                if(r == 0 || c == 0 || r == cells.length-1 || c == cells[0].length-1){
                    cells[r][c].setStato("muro");
                }
            }
        }
    }
    public void mappatura(PacMan bbcman){
        int posRighe = bbcman.getPosR();
        int posColonne = bbcman.getPosC();
        
        resetPassi();
        
        int passi = 0;
        calcolaPassiCellaAdiacente(posRighe, posColonne, passi); 
        cells[posRighe][posColonne].setPassi(0);
    }
    private void resetPassi(){
        for(int r = 0; r < cells.length; r++){
            for(int c = 0; c < cells[0].length; c++){
                cells[r][c].resetPassi();
            }
        }
    }
    
    private void calcolaPassiCellaAdiacente(int posRighe, int posColonne, int passi){
        //imposto il valore di questa come i passi calcolati e incremento di 1
        cells[posRighe][posColonne].setPassi(passi);
        passi++;
        //aggiungo un controllo di out of bound in questo caso sulla colonna massima  
        if(posColonne < cells[0].length-1){ 
            //se non è un muro apro la cella a DESTRA se non è un muro o se la cella ha un num sup uguale a se stessa
            if(!cells[posRighe][posColonne+1].isMuro() && (cells[posRighe][posColonne].getPassi() < cells[posRighe][posColonne+1].getPassi() || cells[posRighe][posColonne+1].getPassi() == 0)) {
                calcolaPassiCellaAdiacente(posRighe, posColonne+1, passi); //incremento la colonna di 1 temporaneo
            }
        }
        //aggiungo un controllo di out of bound in questo caso sulla riga massima  
        if(posRighe < cells.length-1){
            //se non è un muro apro la cella INFERIORE se non è un muro o se la cella ha un num sup uguale a se stessa
            if(!cells[posRighe+1][posColonne].isMuro() && (cells[posRighe][posColonne].getPassi() < cells[posRighe+1][posColonne].getPassi() || cells[posRighe+1][posColonne].getPassi() == 0)) {
                calcolaPassiCellaAdiacente(posRighe+1, posColonne, passi); //incremento la colonna di 1 temporaneo
            }      
        }
        //aggiungo un controllo di out of bound in questo caso sulla riga minima  
        if(posRighe > 0){
            //se non è un muro apro la cella SUPERIORE se non è un muro o se la cella ha un num sup uguale a se stessa
            if(!cells[posRighe-1][posColonne].isMuro() && (cells[posRighe][posColonne].getPassi() < cells[posRighe-1][posColonne].getPassi() || cells[posRighe-1][posColonne].getPassi() == 0)) {
                calcolaPassiCellaAdiacente(posRighe-1, posColonne, passi);
            }      
        }
        //aggiungo un controllo di out of bound in questo caso sulla colonna minima
        if(posColonne > 0){
            //se non è un muro apro la cella SINISTRA se non è un muro o se la cella ha un num sup uguale a se stessa
            if(!cells[posRighe][posColonne-1].isMuro() && (cells[posRighe][posColonne].getPassi() < cells[posRighe][posColonne-1].getPassi() || cells[posRighe][posColonne-1].getPassi() == 0)) {
                calcolaPassiCellaAdiacente(posRighe, posColonne-1, passi);
            }
        }
    }
}
