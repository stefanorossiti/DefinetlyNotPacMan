/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pacman;
/**
 *
 * @author BBC
 */
public class GestioneCibo {
    private Cibo[][] cibi;
    private int cntCiboRimasto;
    
    ///////////////////////////////////////////
    //COSRUTTORI
    ///////////////////////////////////////////
    public GestioneCibo(){

    }
    public GestioneCibo(int r, int c, Griglia g){
        cibi = new Cibo[r][c];
        cntCiboRimasto = r*c;
        for(int i = 0; i < r; i++){
            for(int j = 0; j < c; j++){
                //creo un oggetto cibo alle coordinate
                cibi[i][j] = new Cibo(i, j);
                ////se è già presente un muro lo setto come mangiato altrimenti rimane non mangiato
                if(g.isMuro(i, j)){
                    cibi[i][j].mangiato();
                    cntCiboRimasto--;
                }
            }
        }
    }
    
    ///////////////////////////////////////////
    //SETTER
    ///////////////////////////////////////////
    public void ciboMangiato(int r, int c){
        cibi[r][c].mangiato();
         cntCiboRimasto--;
    }
    public void ciboNonMangiato(int r, int c){
        cibi[r][c].nonMangiato();
        cntCiboRimasto++;
    }
    ///////////////////////////////////////////
    //GETTER
    ///////////////////////////////////////////
    public boolean ciboIsMangiato(int r, int c){
        return cibi[r][c].isMangiato();
    }   
    public int getCiboRimasto(){
        return this.cntCiboRimasto;
    }
    public int getValore(int r, int c){
        return cibi[r][c].getValore();
    }
}
