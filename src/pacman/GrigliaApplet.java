/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * Questo programma è ottimizzato per essere 100x600
 */
package pacman;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Button;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Label;
import java.awt.MediaTracker;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 *
 * @author BBC
 */
public class GrigliaApplet extends Applet implements Runnable, MouseListener, MouseMotionListener, ActionListener, KeyListener {
    ///////////////////////////////////////////
    //INIZIALIZZO LE VARIABILI GENERALI
    ///////////////////////////////////////////

    private int numRighe;
    private int numColonne;						//
    private int lato;
    private int contextW;
    private int contextH;
    private int rCella;
    private int cCella;
    private boolean initPacmanLikeGame = false; //se questa variabile è vera viene inizializzato il gioco in formato pacman 
    private boolean initLoadInitiale = true; //flag di esecuzione del caricamento degli elementi iniziali
    private double punteggio = 0;
    private double punteggioRel = 0; // serve a memorizzare il punteggio gia utilizzato per il calcolo di aumento velocita fantasmini
    private Thread thMovGhosts = new Thread(this);
    private long msMovGhosts = 1500; // iniziano facendo un passo ogni 3 secodni 
    private Thread thWallLamp = new Thread(this);
    ;
    private boolean primoCicloMainMenu = true;
    private Thread thAggMainMenu = new Thread(this);
    private short partenzaPi; //dove randomizzo se iniziale la posizione di pacman a sx a dx  sopra o sotto
    private boolean mainMenuEnabled = true;
    int xPi = -250, yPi = -250; //pos pacman princ inizialmente -250 cosi viene ricalcolata subito dal primo cilco
    private AudioClip songBase; // inizio 
    private AudioClip songYouLoose; // loose
    private AudioClip songMainMenu; // song del menu iniziale
    private AudioClip songYouWin; //song per quando si vince
    private AudioClip songCiboMangiato; //effetto sonoro pewr qunado viene mangiato un pallino
    private AudioClip songMovFant; //song per i fantasmini si muovono
    private boolean initGame = true; //variabile di flag per i cicli di ninit del giogo vero dopo il menu
    private Griglia griglia;
    private PacMan bbcman;
    private GestioneCibo menu;
    private Fantasmino[] ghosts;
    private int ghostsCNT = 0;
    private int maxGhosts = 4;
    private Color bgColor = Color.BLACK;
    private Color wallColor = Color.BLUE;
    private Color streetColor = Color.BLACK;
    Image imgBuff; //dichiarazione oggetti per doppio buffering
    Graphics grafBuff;
    //gioco casual
    private boolean initCasualGame = false;
    private Button bCasualGame;
    // gioco con pacman automatico
    private boolean initAutoPacmanGame = false;
    private boolean autoPacman = false; // viene impostata a true quando parte il gioco con pacman automatico
    // gioco con pacman liv2
    private boolean initPacmanLikeGameL2 = false;
    private Button bStartPacManl2;
    //DICHIARO LE VARIABILI IMG PER I FANTASMINI E PER PACMAN
    Image imgPoweredBy;
    MediaTracker mtPacMan;
    Image imgPacMan;
    MediaTracker mtFant;
    Image imgFantRed;
    Image imgFantOrange;
    Image imgFantBlue;
    Image imgFantPink;
    MediaTracker mtCibo;
    Image imgCiboBase;
    Image imgPi;
    Image imgFiB;
    Image imgFiR;
    Image imgFiG;
    Image imgFiP;
    //Image imgScaledToDraw; //qui memorizzo l immagine scalata in modo da disegnarla
    Image imgLoadingGif;
    ///////////////////////////////////////////
    //INIZIALIZZO GLI ELEMENTI DEL PANNELLINO
    ///////////////////////////////////////////
    private Button bStartPacManl1;
    private Button bStartAutoPacmanl1;
    private Button bAvanti;
    private Button bImposta;
    private TextField tfRighe;
    private TextField tfColonne;
    private Label lRighe;
    private Label lColonne;
    private Label lCntFantasmini;
    private Label lPunteggio;
    ///////////////////////////////////////////
    //INIT
    ///////////////////////////////////////////

    @Override
    public void init() {
        //ingrandisco il conteto grafico e inizio la song del menu
        this.setSize(1000, 599);

        //creazione oggetti per doppio buffering
        imgBuff = createImage(2000, 1000);
        grafBuff = imgBuff.getGraphics();

        //attivo le interfacce per questo applet
        this.addMouseListener(this);
        this.addMouseMotionListener(this);

        //per utilizzare il keylistener devo dare il focus al contesto grafico
        this.setFocusable(true);
        this.requestFocus();

        ///////////////////////////////////////////
        //IMPOSTO GLI ELEMENTI DEL PANNELLINO
        ///////////////////////////////////////////
        bAvanti = new Button("Avanti");     // assegno la label Avanti al pulsante avanti
        bAvanti.addActionListener(this);    // assegno l action listener al pulsante bAvanti
        bStartPacManl1 = new Button("Pacman L1");
        bStartPacManl1.addActionListener(this);
        bStartPacManl2 = new Button("Pacman L2");
        bStartPacManl2.addActionListener(this);
        bStartAutoPacmanl1 = new Button("Auto Pacman");
        bStartAutoPacmanl1.addActionListener(this);
        bCasualGame = new Button("Survival Game");
        bCasualGame.addActionListener(this);
        bAvanti.hide();
        tfRighe = new TextField("11");
        tfColonne = new TextField("20");
        lRighe = new Label("Righe: ");
        lColonne = new Label("Colonne: ");
        bImposta = new Button("Imposta");
        bImposta.addActionListener(this);
        tfRighe.enable(false);
        tfColonne.enable(false);
        bImposta.hide();
        lCntFantasmini = new Label("Fantasmini rimasti: " + (maxGhosts - ghostsCNT));
        lPunteggio = new Label("Punteggio: " + (int) punteggio);
        //lPunteggio.setSize(lPunteggio.getWidth() + 50, lPunteggio.getHeight());

        bbcman = new PacMan();
        ghosts = new Fantasmino[50]; //lo dichiaro piu grande e ridimensiono dopo il massimo di fantasmini che inizialemente è 4
    }

    public void destroy() {
        songBase.stop();
        songMainMenu.stop();
        songYouLoose.stop();
    }

    ///////////////////////////////////////////
    //PAINT
    ///////////////////////////////////////////
    public void update(Graphics g){
        paint(g);
    }

    public void paint(Graphics g) {
        //memorizzo whidth e height del contesto grafico
        //sottraggo 50px in modo da non sovrascrivere il menu
        contextW = this.getWidth();
        contextH = this.getHeight() - 50;

        //Carico tutti gli elementi necessari
        if (initLoadInitiale) {
            //carico immagine loading NON LA CARICA NON SO PERCHE, LA DISEGNA SE TROVA UN ERRORE DOPO G.DWARIMAGE
            grafBuff.fillRect(0, 0, this.getWidth(), this.getHeight());
            imgLoadingGif = getImage(getCodeBase(), "loading.gif");
            grafBuff.drawImage(imgLoadingGif, 200, 100, this);
            g.drawImage(imgBuff, 0, 0, this);

            System.out.print("Caricamento immagini...");
            mtPacMan = new MediaTracker(this);
            mtPacMan.addImage(imgLoadingGif, 0); //per non creare un altro mt lo assegno temporaneamente a quello di pacman
            imgPacMan = getImage(getCodeBase(), "pacmanDx.gif");
            mtPacMan.addImage(imgPacMan, 0);
            imgPi = getImage(getCodeBase(), "pacman.gif");
            mtPacMan.addImage(imgPi, 1);
            mtFant = new MediaTracker(this);
            imgFantRed = getImage(getCodeBase(), "fantR.png");
            imgFantBlue = getImage(getCodeBase(), "fantB.png");
            imgFantOrange = getImage(getCodeBase(), "fantO.png");
            imgFantPink = getImage(getCodeBase(), "fantP.png");
            imgFiB = getImage(getCodeBase(), "fantB.png");
            imgFiR = getImage(getCodeBase(), "fantR.png");
            imgFiG = getImage(getCodeBase(), "fantO.png");
            imgFiP = getImage(getCodeBase(), "fantP.png");
            mtPacMan.addImage(imgFantRed, 0);
            mtPacMan.addImage(imgFantBlue, 1);
            mtPacMan.addImage(imgFantPink, 2);
            mtPacMan.addImage(imgFantOrange, 3);
            mtCibo = new MediaTracker(this);
            imgCiboBase = getImage(getCodeBase(), "ciboBase.gif");
            imgPoweredBy = getImage(getCodeBase(), "poweredBy.gif");
            System.out.println("  OK!");

            //aggiungo i pulsanti di scelta del livello
            this.add(bStartPacManl1);
            this.add(bStartPacManl2);
            this.add(bStartAutoPacmanl1);
            this.add(bCasualGame);

            //carico le song del gioco
            System.out.print("Caricamento tema musicale...");
            songBase = getAudioClip(getCodeBase(), "battle.mid");
            songYouLoose = getAudioClip(getCodeBase(), "dead.mid");
            songMainMenu = getAudioClip(getCodeBase(), "main.mid");
            songYouWin = getAudioClip(getCodeBase(), "win.mid");
            songCiboMangiato = getAudioClip(getCodeBase(), "ciboMangiato.wav");
            songMovFant = getAudioClip(getCodeBase(), "movFant.wav");
            System.out.println("  OK!");
            songMainMenu.loop();

            initLoadInitiale = false;
        }
        //questo ciclo è vero all inizio e stampa il menu
        if (mainMenuEnabled) {
            grafBuff.setColor(bgColor);
            grafBuff.fillRect(0, 35, this.getWidth(), this.getHeight());

            bStartPacManl1.setBackground(Color.YELLOW);
            bStartPacManl1.setForeground(Color.BLUE);
            bStartPacManl1.setBounds(200, 200, 200, 50);
            bStartPacManl2.setBackground(Color.YELLOW);
            bStartPacManl2.setForeground(Color.BLUE);
            bStartPacManl2.setBounds(200, 251, 200, 50);

            bStartAutoPacmanl1.setBackground(Color.YELLOW);
            bStartAutoPacmanl1.setForeground(Color.BLUE);
            bStartAutoPacmanl1.setBounds(600, 200, 200, 50);
            bCasualGame.setBackground(Color.YELLOW);
            bCasualGame.setForeground(Color.BLUE);
            bCasualGame.setBounds(600, 251, 200, 50);

            ///////////////////////////////////////////
            //DISEGNO bbcman 
            ///////////////////////////////////////////
            //se è la prima volta che avvio il menu genero casualmente la partenza del pacman di sfondo e avvio il thread per il suo movimento
            if (primoCicloMainMenu) {
                thAggMainMenu.start();
                primoCicloMainMenu = false;
                partenzaPi = (short) (Math.random() * 4); // 0 = sx, 1 = dx, 2 = sup, 3 = inf
            }
            //disegno i fantasmini iniziali
            if (partenzaPi == 0) {
                //parte a sx
                grafBuff.drawImage(imgFiR, xPi - 50, yPi, this);
                grafBuff.drawImage(imgFiP, xPi - 100, yPi, this);
                grafBuff.drawImage(imgFiG, xPi - 150, yPi, this);
                grafBuff.drawImage(imgFiB, xPi - 200, yPi, this);
            } else if (partenzaPi == 1) {
                //parte a dx
                grafBuff.drawImage(imgFiR, xPi + 50, yPi, this);
                grafBuff.drawImage(imgFiP, xPi + 100, yPi, this);
                grafBuff.drawImage(imgFiG, xPi + 150, yPi, this);
                grafBuff.drawImage(imgFiB, xPi + 200, yPi, this);
            } else if (partenzaPi == 2) {
                //parte sopra
                grafBuff.drawImage(imgFiR, xPi, yPi - 50, this);
                grafBuff.drawImage(imgFiP, xPi, yPi - 100, this);
                grafBuff.drawImage(imgFiG, xPi, yPi - 150, this);
                grafBuff.drawImage(imgFiB, xPi, yPi - 200, this);
            } else {
                //parte sotto
                grafBuff.drawImage(imgFiR, xPi, yPi + 50, this);
                grafBuff.drawImage(imgFiP, xPi, yPi + 100, this);
                grafBuff.drawImage(imgFiG, xPi, yPi + 150, this);
                grafBuff.drawImage(imgFiB, xPi, yPi + 200, this);
            }
            //disegno il pacman
            grafBuff.drawImage(imgPacMan, xPi + 5, yPi + 5, this);
            //stampo il powered by
            grafBuff.drawImage(imgPoweredBy, 380, 540, this);
            g.drawImage(imgBuff, 0, -35, this);
            //menu non abilitato quindi stampa il gioco
        } else {
            //ripulisco lo schermo
            grafBuff.setColor(bgColor);
            grafBuff.fillRect(0, 0, this.getWidth(), this.getHeight());

            //nascondo i pulsanti del menu tranne quello del gioco scelto che però disabilito cosi viene visto nel menu in alto nel gioco
            if (!initPacmanLikeGame) {
                bStartPacManl1.hide();
            } else {
                bStartPacManl1.disable();
            }
            if (!initPacmanLikeGameL2) {
                bStartPacManl2.hide();
            } else {
                bStartPacManl2.disable();
            }
            if (!initAutoPacmanGame) {
                bStartAutoPacmanl1.hide();
            } else {
                bStartAutoPacmanl1.disable();
            }
            if (!initCasualGame) {
                bCasualGame.hide();
            } else {
                bCasualGame.disable();
            }

            if (initPacmanLikeGame) {
                //Trovo righe e colonne necessarie
                numRighe = Integer.parseInt(tfRighe.getText());
                numColonne = Integer.parseInt(tfColonne.getText());

                this.setSize(1000, 600);
                this.removeMouseListener(this);
                this.removeMouseMotionListener(this);
                //bgColor = Color.BLACK;
                //wallColor = Color.BLUE;
                streetColor = bgColor;
                griglia = new Griglia(numRighe, numColonne);
                griglia.generaMuriPacman();
                menu = new GestioneCibo(numRighe, numColonne, griglia);
                ghosts[ghostsCNT] = new Fantasmino(5, 8);
                ghosts[ghostsCNT].setColor(Color.PINK);
                ghostsCNT++;
                ghosts[ghostsCNT] = new Fantasmino(5, 9);
                ghosts[ghostsCNT].setColor(Color.RED);
                ghostsCNT++;
                ghosts[ghostsCNT] = new Fantasmino(5, 10);
                ghosts[ghostsCNT].setColor(Color.BLUE);
                ghostsCNT++;
                ghosts[ghostsCNT] = new Fantasmino(5, 11);
                ghosts[ghostsCNT].setColor(Color.ORANGE);
                ghostsCNT++;
                bbcman.setPos(7, 10);
                calcolaMappatura(); //calcolo il numerino che indica i passi per ogni cella   
                thMovGhosts.start();

                initPacmanLikeGame = false;
            } else if (initPacmanLikeGameL2) {
                this.setSize(1200, 800);
                //Trovo righe e colonne necessarie
                numRighe = Integer.parseInt(tfRighe.getText());
                numColonne = Integer.parseInt(tfColonne.getText());

                this.removeMouseListener(this);
                this.removeMouseMotionListener(this);
                //bgColor = Color.BLACK;
                //wallColor = Color.BLUE;
                streetColor = bgColor;
                griglia = new Griglia(numRighe, numColonne);
                griglia.generaMuriPacmanL2();
                menu = new GestioneCibo(numRighe, numColonne, griglia);
                ghosts[ghostsCNT] = new Fantasmino(8, 9);
                ghosts[ghostsCNT].setColor(Color.PINK);
                ghostsCNT++;
                ghosts[ghostsCNT] = new Fantasmino(8, 10);
                ghosts[ghostsCNT].setColor(Color.RED);
                ghostsCNT++;
                ghosts[ghostsCNT] = new Fantasmino(8, 11);
                ghosts[ghostsCNT].setColor(Color.BLUE);
                ghostsCNT++;
                ghosts[ghostsCNT] = new Fantasmino(8, 12);
                ghosts[ghostsCNT].setColor(Color.ORANGE);
                ghostsCNT++;
                bbcman.setPos(10, 10);
                calcolaMappatura(); //calcolo il numerino che indica i passi per ogni cella   
                thMovGhosts.start();

                initPacmanLikeGameL2 = false;
            } else if (initAutoPacmanGame) {
                this.setSize(600, 600);
                //Trovo righe e colonne necessarie
                numRighe = Integer.parseInt(tfRighe.getText());
                numColonne = Integer.parseInt(tfColonne.getText());
                streetColor = bgColor;
                griglia = new Griglia(numRighe, numColonne);

                initAutoPacmanGame = false;
            } else if (initCasualGame) {
                this.setSize(800, 600);
                //Trovo righe e colonne necessarie
                numRighe = Integer.parseInt(tfRighe.getText());
                numColonne = Integer.parseInt(tfColonne.getText());
                streetColor = bgColor;
                griglia = new Griglia(numRighe, numColonne);
                griglia.generaMuriBordo();
                bbcman.setPos(1, 1);
                ghosts[ghostsCNT] = new Fantasmino(numRighe - 2, numColonne - 2);
                ghostsCNT++;
                thMovGhosts.start();
                maxGhosts = 50;

                initCasualGame = false;
            }

            if (initGame) {
                this.addKeyListener(this);
                //aggiungo i pulsanti al contesto grafico      
                //this.add(bAvanti);
                this.add(lColonne);
                this.add(tfColonne);
                this.add(lRighe);
                this.add(tfRighe);
                this.add(lCntFantasmini);
                this.add(lPunteggio);
                //lPunteggio.setBounds(lPunteggio.getX(), lPunteggio.getY(), lPunteggio.getWidth() + 15, lPunteggio.getHeight()); // NON VA
                //richiedo il focus per il funzionamento di keylistener
                this.requestFocus();
                //inizio la canzone di sottofondo del gioco
                songMainMenu.stop();
                songBase.loop();
                initGame = false;
            }
            //coloro il background del campo
            grafBuff.setColor(bgColor);
            grafBuff.fillRect(0, 35, this.getWidth(), this.getHeight());

            //riscrivo il coteggio fantasmini nel menu superiore
            lCntFantasmini.setText("Num fantasmini: " + ghostsCNT);
            lPunteggio.setText("Punteggio: " + (int) punteggio);

            ///////////////////////////////////////////
            //DECIDO IL LATO DEL QUDRATO IN MODOD OTTIMALE
            //In piu decido gli offset per la centratura
            ///////////////////////////////////////////
            if (numRighe > numColonne && contextH > contextW) {
                lato = contextW / numColonne;
                if (lato * numRighe > contextH) {
                    lato = contextH / numRighe;
                }
            } else if (numRighe > numColonne && contextH < contextW) {
                lato = contextH / numRighe;
            } else if (numRighe < numColonne && contextH > contextW) {
                lato = contextW / numColonne;
            } else { //righe < colonne && contH < contW
                lato = contextH / numRighe;
                if (lato * numColonne > contextW) {
                    lato = contextW / numColonne;
                }
            }
            ///////////////////////////////////////////
            //DISEGNO LA GRIGLIA DI BASE CON CELLE
            //VUOTE O MURI
            ///////////////////////////////////////////
            grafBuff.drawLine(0, 35, this.getWidth(), 35);
            for (int r = 0; r < numRighe; r++) {
                for (int c = 0; c < numColonne; c++) {
                    if (!griglia.isMuro(r, c)) {
                        grafBuff.setColor(streetColor);
                        grafBuff.fillRect(offsetX() + (c * lato), offsetY() + (r * lato), lato, lato);
                    } else {
                        grafBuff.setColor(wallColor);
                        grafBuff.fill3DRect(offsetX() + (c * lato), offsetY() + (r * lato), lato, lato, true);
                    }
                    //stampo il numero di Passi nella cella
                    //grafBuff.setColor(Color.DARK_GRAY);
                    //grafBuff.drawString(Integer.toString(griglia.getPassi(r, c)), offsetX() + (c*lato), 10 + offsetY() + (r*lato));
                }
            }

            ///////////////////////////////////////////
            //DISEGNO LA GRIGLIA DI BASE CON CELLE
            //VUOTE O MURI
            ///////////////////////////////////////////
            grafBuff.drawLine(0, 35, this.getWidth(), 35);
            for (int r = 0; r < numRighe; r++) {
                for (int c = 0; c < numColonne; c++) {
                    if (!griglia.isMuro(r, c)) {
                        grafBuff.setColor(streetColor);
                        grafBuff.fillRect(offsetX() + (c * lato), offsetY() + (r * lato), lato, lato);
                    } else {
                        grafBuff.setColor(wallColor);
                        grafBuff.fillRect(offsetX() + (c * lato), offsetY() + (r * lato), lato, lato);
                    }
                    //stampo il numero di Passi nella cella
                    //grafBuff.setColor(Color.DARK_GRAY);
                    //grafBuff.drawString(Integer.toString(griglia.getPassi(r, c)), offsetX() + (c*lato), 10 + offsetY() + (r*lato));
                }
            }
            ///////////////////////////////////////////
            //DISEGNO IL CIBO
            ///////////////////////////////////////////           
            //non viene eseguito se autopacman è in aseguzione quindi si gioca senza cibo
            if (!autoPacman) {
                for (int r = 0; r < numRighe; r++) {
                    for (int c = 0; c < numColonne; c++) {
                        if (!autoPacman && !menu.ciboIsMangiato(r, c)) {
                            grafBuff.drawImage(imgCiboBase, offsetX() + (c * lato) + lato / 2, offsetY() + (r * lato) + lato / 2, this);
                        }
                    }
                }
            }
            ///////////////////////////////////////////
            //DISEGNO I FANTASMINI
            ///////////////////////////////////////////
            for (int i = 0; i < ghostsCNT; i++) {
                if (ghosts[i].getColor() == Color.RED) {
                    //imgScaledToDraw = imgFantRed.getScaledInstance(100, 100, Image.SCALE_FAST); PROBLEMA: //http://stackoverflow.com/questions/7252983/resizing-image-java-getscaledinstance
                    grafBuff.drawImage(imgFantRed, offsetX() + lato * ghosts[i].getPosC(), offsetY() + lato * ghosts[i].getPosR(), this);
                } else if (ghosts[i].getColor() == Color.BLUE) {
                    grafBuff.drawImage(imgFantBlue, offsetX() + lato * ghosts[i].getPosC(), offsetY() + lato * ghosts[i].getPosR(), this);
                } else if (ghosts[i].getColor() == Color.ORANGE) {
                    grafBuff.drawImage(imgFantOrange, offsetX() + lato * ghosts[i].getPosC(), offsetY() + lato * ghosts[i].getPosR(), this);
                } else if (ghosts[i].getColor() == Color.PINK) {
                    grafBuff.drawImage(imgFantPink, offsetX() + lato * ghosts[i].getPosC(), offsetY() + lato * ghosts[i].getPosR(), this);
                } else {
                    System.out.println("Errore nel disegno del fantasmino: " + i);
                }
            }
            ///////////////////////////////////////////
            //DISEGNO bbcman 
            ///////////////////////////////////////////
            grafBuff.drawImage(imgPacMan, offsetX() + lato * bbcman.getPosC() + 8, offsetY() + lato * bbcman.getPosR() + 8, this);

            //visualizzo l og image creata
            g.drawImage(imgBuff, 0, 0, this);

            //Quando si perde
            if (youLoose() && thMovGhosts.isAlive()) {
                songBase.stop();
                thMovGhosts.stop();
                thWallLamp.start();
                System.out.println("GAMEOVER!");
                this.removeKeyListener(this);
                songYouLoose.play();
            }
            //Quando si vince
            if (youWin() && thMovGhosts.isAlive()) {
                songBase.stop();
                thMovGhosts.stop();
                thWallLamp.start();
                System.out.println("YOUWIN!");
                this.removeKeyListener(this);
                songYouWin.play();
            }
        }
    }
    ///////////////////////////////////////////
    //METODI GENERALI
    /////////////////////////////////////////// 
    //controlla se si è stati mangiati dai mantasmini quindi si ha perso

    private boolean youLoose() {
        if (cellaSenzaFant(bbcman.getPosR(), bbcman.getPosC())) {
            return false;
        } else {
            return true;
        }
    }
    //controlla se si ha mangiato tutti i cibi quindi si ha vinto

    private boolean youWin() {
        if (!autoPacman && menu.getCiboRimasto() == 0) {
            return true;
        } else {
            return false;
        }
    }

    private int offsetX() {
        return (contextW - (lato * numColonne)) / 2;
    }

    private int offsetY() {
        return (50 + (((contextH) - (numRighe * lato)) / 2)) - 1;
    }

    private void calcolaMappatura() {
        griglia.mappatura(bbcman);
    }
    //setta le variabili globali rCella e cCella 

    private void trovaCella(int coordX, int coordY) {
        //riga cliccata esegue solo se si ha cliccato sulla griglia
        if (coordY > offsetY() && coordY < offsetY() + lato * numRighe) {
            for (int i = 0; i < numRighe; i++) {
                if (offsetY() + lato * i < coordY) {
                    rCella = i;
                }
            }
        }
        //colonna cliccata esegue solo se si ha cliccato sulla griglia
        if (coordX > offsetX() && coordX < offsetX() + lato * numColonne) {
            for (int i = 0; i < numColonne; i++) {
                if (offsetX() + lato * i < coordX) {
                    cCella = i;
                }
            }
        }
    }
    //ritorna true se nella cella non ci sono muri

    private boolean cellaSenzaMuri(int r, int c) {
        if (griglia.isMuro(r, c)) {
            return false;
        } else {
            return true;
        }
    }
    //ritorna true se nella cella cliccata non è gia presente un fantasmino oppure pacman

    private boolean cellaIsLibera(MouseEvent e) {
        trovaCella(e.getX(), e.getY());
        //controllo se la cella cliccata contine un pacman
        if (rCella == bbcman.getPosR() && cCella == bbcman.getPosC()) {
            return false;
        } //controllo che non ci siano altri fantasmini
        else if (!cellaSenzaFant(rCella, cCella)) {
            return false;
        } else {
            return menu.ciboIsMangiato(rCella, cCella);
        }
    }
    //controlla che nella cella passata non ci sia nessun fantasmino NON FUNZIONA AL 100% in muoviFantasmini()

    private boolean cellaSenzaFant(int nR, int nC) {
        for (int iFa = 0; iFa < ghostsCNT; iFa++) {
            if (ghosts[iFa].getPosC() == nC && ghosts[iFa].getPosR() == nR) {
                return false;
            }
        }
        return true;
    }

    private void muoviFantasmini() {
        //Calcolo il prossimo passo di ogni singolo 
        for (int iF = 0; iF < ghostsCNT; iF++) {
            int rFant = ghosts[iF].getPosR();
            int cFant = ghosts[iF].getPosC();

            //ogni if ontrolla anche se non si va aout of bound
            //se è possibile vado a destra, c+1
            //pois dopo controllo che non ci siano gia altri fantasmini in quella cella se no non avanza li
            if (cFant < numColonne - 1 && !griglia.isMuro(rFant, cFant + 1) && griglia.getPassi(rFant, cFant) > griglia.getPassi(rFant, cFant + 1) && cellaSenzaFant(rFant, cFant + 1)) {
                ghosts[iF].goRight();
                punteggio += 50;
            } //se è possibile vado a sinistra, c-1
            else if (cFant > 0 && !griglia.isMuro(rFant, cFant - 1) && griglia.getPassi(rFant, cFant) > griglia.getPassi(rFant, cFant - 1) && cellaSenzaFant(rFant, cFant - 1)) {
                ghosts[iF].goLeft();
                punteggio += 50;
            } //se è possibile vado in giu, r+1
            else if (rFant < numRighe - 1 && !griglia.isMuro(rFant + 1, cFant) && griglia.getPassi(rFant, cFant) > griglia.getPassi(rFant + 1, cFant) && cellaSenzaFant(rFant + 1, cFant)) {
                ghosts[iF].goDown();
                punteggio += 50;
            } //se è possibile vado in alto, r-1
            else if (rFant > 0 && !griglia.isMuro(rFant - 1, cFant) && griglia.getPassi(rFant, cFant) > griglia.getPassi(rFant - 1, cFant) && cellaSenzaFant(rFant - 1, cFant)) {
                ghosts[iF].goUp();
                punteggio += 50;
            } else {
            }
        }
        songMovFant.play();
    }

    public void run() {
        //avvicino i fantasmini 
        while (thMovGhosts.isAlive()) {
            //se necessario aumento velocità
            if (Math.floor((punteggio - punteggioRel) / 1000) > 0 && msMovGhosts > 400) {
                //se il punteggio è multiplo di 100 diminuisco il delay tra un passo e l altro 
                //di 200ms per un minimo di 400ms, visto che il calcolo viene fatto solo
                //quando i fantasmini si muovono, nel frattempo è possbile fare 
                //piu punti di un ultiplo di 100 quindi lo tengo presento nel calcolo dopo
                // in piu aggiungo un fantasmino
                msMovGhosts -= 100 * Math.floor((punteggio - punteggioRel) / 1000);
                punteggioRel = punteggio;

            }
            //se non ho raggiunto il numero massimo di fantasmini ne aggiungo 1
            //questa aggiunta avviene ogni volta che viene eseguito un passo e il punteggio è multiplo di: 100 per il num di fantasmini gia presenti + 1000 * num di fantasmini
            if (ghostsCNT < maxGhosts && punteggio / (500 * Math.pow(ghostsCNT, 2)) > 1) {
                //genero un fantasmino a una coordinata casuale che non è un muro o non ce pacman
                int rNF;
                int cNF;
                do {
                    rNF = (int) (Math.random() * numRighe - 2) + 1;
                    cNF = (int) (Math.random() * numColonne - 2) + 1;

                } while (griglia.isMuro(rNF, cNF) || (rNF == bbcman.getPosR() && cNF == bbcman.getPosC()));

                ghosts[ghostsCNT] = new Fantasmino(rNF, cNF);
                ghostsCNT++;
                System.out.println("fant n: " + ghostsCNT);
            }

            try {
                Thread.sleep(msMovGhosts);
                muoviFantasmini();
                repaint();
            } catch (InterruptedException e) {
                System.out.println("InterruptedExceptionGhost");
            }
        }

        //togglo lo stato dei muri
        while (thWallLamp.isAlive()) {
            //togla lo stato della cella se non vi è dentro un pacman o una fantasmino 
            for (int r = 0; r < numRighe; r++) {
                for (int c = 0; c < numColonne; c++) {
                    if (!(r == bbcman.getPosR() && c == bbcman.getPosC()) && cellaSenzaFant(r, c)) {
                        griglia.toggleStato(r, c);
                    }
                }
            }
            try {
                Thread.sleep(600);
                repaint();
            } catch (InterruptedException e) {
                System.out.println("InterruptedExceptionWall");
            }
        }
        //togglo lo stato dei muri
        while (thAggMainMenu.isAlive()) {
            //La seguente struttura if serve a generre la posizione iniziale del pacman del menu
            if (xPi == 0 - 250 || xPi == this.getWidth() + 200 || yPi == 0 - 200 || yPi == this.getHeight() + 200) {
                partenzaPi = (short) (Math.round(Math.random() * 4)); // 0 = sx, 1 = dx, 2 = sup, 3 = inf
                if (partenzaPi == 0) {
                    //parte a sx
                    xPi = 1;
                    yPi = (int) ((Math.random() + 0.1) * (this.getHeight() - 200));
                    imgPacMan = getImage(getCodeBase(), "pacmanDx.gif");
                } else if (partenzaPi == 1) {
                    //parte a dx
                    xPi = this.getWidth() - 1;
                    yPi = (int) ((Math.random() + 0.1) * (this.getHeight() - 200));
                    imgPacMan = getImage(getCodeBase(), "pacmanSx.gif");
                } else if (partenzaPi == 2) {
                    //parte sopra
                    xPi = (int) ((Math.random() + 0.1) * (this.getWidth() - 200));
                    yPi = 1;
                    imgPacMan = getImage(getCodeBase(), "pacmanGiu.gif");
                } else {
                    //parte sotto
                    xPi = (int) ((Math.random() + 0.1) * (this.getWidth() - 200));
                    yPi = this.getHeight() - 1;
                    imgPacMan = getImage(getCodeBase(), "pacmanUp.gif");
                }
            }
            try {
                Thread.sleep(5);
                if (partenzaPi == 0) {
                    //parte a sx
                    xPi += 1;
                } else if (partenzaPi == 1) {
                    //parte a dx
                    xPi -= 1;
                } else if (partenzaPi == 2) {
                    //parte sopra
                    yPi += 1;
                } else {
                    //parte sotto
                    yPi -= 1;
                }
                repaint();
            } catch (InterruptedException e) {
                System.out.println("InterruptedExceptionWall");
            }
        }
    }

    ///////////////////////////////////////////
    //METODI INTERFACE UTILIZZATI
    ///////////////////////////////////////////
    public void mouseDragged(MouseEvent e) {
        if (thMovGhosts.isAlive() && e.getX() > offsetX() && e.getX() < offsetX() + lato * numColonne && e.getY() > offsetY() && e.getY() < offsetY() + lato * numRighe) {
            if (e.getModifiers() == MouseEvent.BUTTON1_MASK) {
                if (autoPacman || cellaIsLibera(e)) {
                    trovaCella(e.getX(), e.getY());
                    griglia.setStato(rCella, cCella, "muro");
                    calcolaMappatura();
                    repaint();
                }
            }
        }
    }

    public void mouseClicked(MouseEvent e) {
        //imposto il focus all applet se premo nella parte sotto il menu
        if (e.getX() > 50) {
            this.requestFocus();
        }
        trovaCella(e.getX(), e.getY());
        //esegue solo se si ha cliccato sulla griglia, creo o cancello muro oppure se sto eseguento il gioco con autopacman
        if (autoPacman || cellaIsLibera(e)) {
            if (thMovGhosts.isAlive() && e.getX() > offsetX() && e.getX() < offsetX() + lato * numColonne && e.getY() > offsetY() && e.getY() < offsetY() + lato * numRighe) {
                //se clicco col sx
                //togglo lo stato della cella
                //in piu controllo se il thread principale del gioco è attivo
                if (e.getButton() == MouseEvent.BUTTON1) {
                    griglia.toggleStato(rCella, cCella);
                    calcolaMappatura(); //ricalcolo la mappatura
                    System.out.println("clicksx " + rCella + " " + cCella);
                    repaint();
                }
                if (!griglia.isMuro(rCella, cCella)) {
                    //se clicco con la rotella del mouse e se non è gia muro, sposto pacman
                    if (thMovGhosts.isAlive() && e.getButton() == MouseEvent.BUTTON2) {
                        bbcman.setPos(rCella, cCella);
                        calcolaMappatura(); //ricalcolo la mappatura
                        repaint();
                    }
                    //se clicco con tasto dx e non è gia muro creao un fantasmino
                    //e se non si ha raggiunto il num massimo di fantasmino
                    if (thMovGhosts.isAlive() && e.getButton() == MouseEvent.BUTTON3 && ghostsCNT < maxGhosts) {
                        ghosts[ghostsCNT] = new Fantasmino(rCella, cCella);
                        ghostsCNT++;
                        repaint();
                    }
                }
            }
        }
    }

    public void keyTyped(KeyEvent e) {

        if (e.getKeyChar() == 'a' || e.getKeyChar() == 'A') {
            //aggiungo i controlli di limite della griglia e muri
            if (bbcman.getPosC() - 1 >= 0) {
                if (!griglia.isMuro(bbcman.getPosR(), bbcman.getPosC() - 1)) {
                    bbcman.goLeft();
                    imgPacMan = getImage(getCodeBase(), "pacmanSx.gif");
                    //setto come mangiato i pallino presente nella nuova posizione di pacman
                    //eseguo il suono di nutrimento
                    if (!autoPacman && !menu.ciboIsMangiato(bbcman.getPosR(), bbcman.getPosC())) {
                        menu.ciboMangiato(bbcman.getPosR(), bbcman.getPosC());
                        songCiboMangiato.play();
                        punteggio += menu.getValore(bbcman.getPosR(), bbcman.getPosC());
                    }
                    calcolaMappatura(); //ricalcolo la mappatura
                    repaint();
                }
            }
        } else if (e.getKeyChar() == 's' || e.getKeyChar() == 'S') {
            //aggiungo i controlli di limite della griglia e muri
            if (bbcman.getPosR() + 1 < numRighe) {
                if (!griglia.isMuro(bbcman.getPosR() + 1, bbcman.getPosC())) {
                    bbcman.goDown();
                    imgPacMan = getImage(getCodeBase(), "pacmanGiu.gif");
                    if (!autoPacman && !menu.ciboIsMangiato(bbcman.getPosR(), bbcman.getPosC())) {
                        menu.ciboMangiato(bbcman.getPosR(), bbcman.getPosC());
                        songCiboMangiato.play();
                        punteggio += menu.getValore(bbcman.getPosR(), bbcman.getPosC());
                    }
                    calcolaMappatura(); //ricalcolo la mappatura
                    repaint();
                }
            }
        }
        if (e.getKeyChar() == 'd' || e.getKeyChar() == 'D') {
            //aggiungo i controlli di limite della griglia e muri
            if (bbcman.getPosC() + 1 < numColonne) {
                if (!griglia.isMuro(bbcman.getPosR(), bbcman.getPosC() + 1)) {
                    bbcman.goRight();
                    imgPacMan = getImage(getCodeBase(), "pacmanDx.gif");
                    if (!autoPacman && !menu.ciboIsMangiato(bbcman.getPosR(), bbcman.getPosC())) {
                        menu.ciboMangiato(bbcman.getPosR(), bbcman.getPosC());
                        songCiboMangiato.play();
                        punteggio += menu.getValore(bbcman.getPosR(), bbcman.getPosC());
                    }
                    calcolaMappatura(); //ricalcolo la mappatura
                    repaint();
                }
            }
        } else if (e.getKeyChar() == 'w' || e.getKeyChar() == 'W') {
            //aggiungo i controlli di limite della griglia e muri
            if (bbcman.getPosR() - 1 >= 0) {
                if (!griglia.isMuro(bbcman.getPosR() - 1, bbcman.getPosC())) {
                    bbcman.goUp();
                    imgPacMan = getImage(getCodeBase(), "pacmanUp.gif");
                    if (!autoPacman && !menu.ciboIsMangiato(bbcman.getPosR(), bbcman.getPosC())) {
                        menu.ciboMangiato(bbcman.getPosR(), bbcman.getPosC());
                        songCiboMangiato.play();
                        punteggio += menu.getValore(bbcman.getPosR(), bbcman.getPosC());
                    }
                    calcolaMappatura(); //ricalcolo la mappatura
                    repaint();
                }
            }
        } else {//fa niente
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == bStartPacManl1) {
            mainMenuEnabled = false;
            initPacmanLikeGame = true;
            tfRighe.setText("11");
            tfColonne.setText("20");
            thAggMainMenu.stop();
            repaint();
        }
        if (e.getSource() == bStartPacManl2) {
            tfRighe.setText("16");
            tfColonne.setText("21");
            mainMenuEnabled = false;
            initPacmanLikeGameL2 = true;
            thAggMainMenu.stop();
            repaint();
        }
        if (e.getSource() == bStartAutoPacmanl1) {
            mainMenuEnabled = false;
            initAutoPacmanGame = true;
            autoPacman = true;
            tfRighe.setText("10");
            tfColonne.setText("10");
            thAggMainMenu.stop();
            repaint();
        }
        if (e.getSource() == bCasualGame) {
            mainMenuEnabled = false;
            initCasualGame = true;
            autoPacman = true; // non è verò pero almeno non carica i cibi
            tfRighe.setText("11");
            tfColonne.setText("14");
            thAggMainMenu.stop();
            repaint();
        }
    }

    ///////////////////////////////////////////
    //METODI INTERFACE NON UTILIZZATI
    ///////////////////////////////////////////
    public void mouseMoved(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mousePressed(MouseEvent e) {
    }

    public void keyPressed(KeyEvent e) {
    }

    public void keyReleased(KeyEvent e) {
    }
}