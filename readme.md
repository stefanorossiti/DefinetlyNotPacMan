# Definitely Not PacMan

Latest version: v1.2.0

![ezgif-2-75e5b259bd.gif](images/ezgif-2-75e5b259bd.gif)

## Description

This project started with the study of the Manhattan algorithm which is the 
AI used by the ghosts to find the shortest path to reach PacMan.

You can move PacMan with the keyboard `wasd` pattern.

This is the first map (L1):

![Scheme](images/notpacmanl1.png)

The second map is based on pacman 2 (L2):

![L2.png](images/L2.png)

The third option the `survival game`:

![survuv.png](images/survuv.png)

This is a different modality where you add and remove the blocks of map.
THe goal is to survive as long as possible. With time, the game gets harder 
by adding ghosts, removing blocks and increasing the speed of the ghosts.

I never implemented The `Auto pacman` modality, which was meant to program 
the pacman to play by itself. And survive the longest possible. 

## ChangeLog

[CHANGELOG](CHANGELOG)

## Requirements

- Java (6 to) 8 (max, recommended)
- NetBeans (highly recommended)

Java Applets have been dropped with Java 9. So you need an older versione to run this project.
Java 8 is the preferred version. Until Java 6 should be ok.

Problems running the project are expected, applets are deprecated and not supported by modern browsers.**

This is a NetBeans Project. Using NetBeans isn't mandatory but recommended for the built-in applet runner.
You can run the GrigliaApplet.java class with netbeans.

[NetBeans Instructions](https://netbeans.apache.org/tutorial/main/kb/docs/java/javase-jdk8/)

Alternatively, you can use IntellijJ IDEA with the [Applet runner plugin](https://plugins.jetbrains.com/plugin/16682-applet-runner) (not tested).

## Installation and run

Clone the repo:

```bash
git clone https://stefanorossi@bitbucket.org/stefanorossi/definetlynotpacman.git
```

Run `SuppaMarioApplet.java` with your preferred IDE (NetBeans is recommended) using java 8.

## License 

All my work is released under [DBAD](https://www.dbad-license.org/) license.
